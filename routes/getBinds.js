require('dotenv').config();

const connection = require('../routes/bdConn');

module.exports = function (app) {

    //получить список связей пользователей и групп
    app.get('/users_bind', (req, res) => {
        connection.query(`select users_bind.uid, users.login, users_bind.gid, groups.title from ${process.env.DB_NAME}.users_bind left join ${process.env.DB_NAME}.users on ( users_bind.uid = users.id ) left join ${process.env.DB_NAME}.groups on ( users_bind.gid = groups.id );`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    //получить список связей групп и подписок
    app.get('/groups_subscriptions', (req, res) => {
        connection.query(`select groups.title, subscriptions.desc, groups_subscribe.date from ${process.env.DB_NAME}.groups_subscribe left join ${process.env.DB_NAME}.groups on ( groups_subscribe.gid = groups.id ) left join ${process.env.DB_NAME}.subscriptions on ( groups_subscribe.sid = subscriptions.id );`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    //получить список связей пакетов и курсов
    app.get('/courses_pack', (req, res) => {
        connection.query(`select courses_pack.excluded_works, courses.title as title1, packages.title as title2 from ${process.env.DB_NAME}.courses_pack left join ${process.env.DB_NAME}.courses on ( courses_pack.cid = courses.id ) left join ${process.env.DB_NAME}.packages on ( courses_pack.pid = packages.id );`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    //получить список связей пакетов и расписания
    app.get('/subscriptions', (req, res) => {
        connection.query(`SELECT packages.title, schedules.desc as desc1, subscriptions.desc as desc2 from ${process.env.DB_NAME}.subscriptions left join ${process.env.DB_NAME}.packages on ( subscriptions.pid = packages.id ) left join ${process.env.DB_NAME}.schedules on ( subscriptions.sid = schedules.id );`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

};