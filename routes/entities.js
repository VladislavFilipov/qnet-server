require('dotenv').config();
const connection = require('../routes/bdConn');

module.exports = function (app) {
    //получить список пользователей
    app.get('/users', (req, res) => {
        connection.query(`select id, login, registered from users;`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                // console.log(results);
                res.send(results);
            }

        });
    });

    app.get('/works', (req, res) => {
        connection.query(`select works.id, works.title, works.desc from ${process.env.DB_NAME}.works;`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    //получить список групп
    app.get('/groups', (req, res) => {
        connection.query(`select id, title from ${process.env.DB_NAME}.groups;`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    // получить список подписок
    app.get('/subs', (req, res) => {
        connection.query(`select id, subscriptions.desc from subscriptions;`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    // получить список расписаний
    app.get('/schedules', (req, res) => {
        connection.query(`select id, schedules.duration, schedules.desc from schedules;`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    // получить список пакетов
    app.get('/packages', (req, res) => {
        connection.query(`select id, packages.title, packages.desc from ${process.env.DB_NAME}.packages;`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    // получить список курсов
    app.get('/courses', (req, res) => {
        connection.query(`select id, courses.title, courses.desc, courses.numb from ${process.env.DB_NAME}.courses;`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    //Получить список заданий к лабам
    app.get('/exercises', (req, res) => {
        connection.query(`select id, title, content from exercises;`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    // получить все данные
    app.get('/allData', (req, res) => {
        connection.query(`select groups.title as gtitle, groups.desc as gdesc, schedules.desc as sdesc, schedules.duration, schedules_times.weekday, schedules_times.tbeg, schedules_times.tend, packages.title as ptitle from ${process.env.DB_NAME}.groups_subscribe left join ${process.env.DB_NAME}.groups on (groups_subscribe.gid = groups.id) left join ${process.env.DB_NAME}.subscriptions on (groups_subscribe.sid = subscriptions.id) left join schedules on (subscriptions.sid = schedules.id) left join schedules_times on (schedules_times.sid = schedules.id) left join ${process.env.DB_NAME}.packages on (subscriptions.pid = packages.id);`, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });
};