const verifyTokens = require('../auth/verifyTokens');
const connection = require('../routes/bdConn');

require('dotenv').config();

module.exports = function (app) {

    // добавить расписание
    app.post('/schedules/add', verifyTokens, (req, res) => {
        const schedule = req.body.schedule;

        const query1 = `insert into ${process.env.DB_NAME}.schedules(schedules.duration, schedules.desc) values (${schedule.duration}, '${schedule.description}'); `;
        const query2 = `select id from ${process.env.DB_NAME}.schedules where schedules.duration = ${schedule.duration} and schedules.desc = '${schedule.description}';`;

        connection.query(query1, (err, results, fields) => {
            if (err) {
                console.log(err);
                res.send({ 'error': err });
            } else {
                const lastId = results.insertId;

                connection.query(query2, (err, results, fields) => {
                    if (err) {
                        console.log(err);
                        res.send({ 'error': err });
                    } else {
                        let query3 = `insert into ${process.env.DB_NAME}.schedules_times (schedules_times.sid , schedules_times.weekday , schedules_times.tbeg , schedules_times.tend) values `;

                        schedule.weekdays.forEach(weekday => {
                            schedule.times.forEach(time => {
                                query3 += `(${lastId}, '${weekday}', '${time.tbeg}', '${time.tend}'), `
                            });
                        });

                        query3 = query3.slice(0, query3.length - 2) + ';';

                        connection.query(query3, (err, results2, fields) => {
                            if (err) {
                                console.log(err);
                                res.send({ 'error': err });
                            } else {
                                res.send(results2);
                            }
                        });
                    }
                });
            }
        });
    });

    // добавить расписание
    app.post('/schedules/update', verifyTokens, (req, res) => {
        const schedule = req.body.schedule;

        let query = `insert into ${process.env.DB_NAME}.schedules_times (schedules_times.sid , schedules_times.weekday , schedules_times.tbeg , schedules_times.tend) values `;

        schedule.weekdays.forEach(weekday => {
            schedule.times.forEach(time => {
                query += `(${schedule.id}, '${weekday}', '${time.tbeg}', '${time.tend}'), `
            });
        });

        query = query.slice(0, query.length - 2) + ';';

        connection.query(query, (err, results2, fields) => {
            if (err) {
                console.log(err);
                res.send({ 'error': err });
            } else {
                res.send(results2);
            }
        });
    });
};