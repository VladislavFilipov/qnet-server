require('dotenv').config();

const entities = require('./entities');
const bind = require('./bind');
const getBinds = require('./getBinds');
const auth = require('../auth');
const schedule = require('./schedule');
const package = require('./package');

module.exports = function (app) {

    app.get('/', (req, res) => {
        res.send('test');
    });

    auth(app);
    entities(app);
    bind(app);
    getBinds(app);
    schedule(app);
    package(app);
}