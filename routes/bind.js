require('dotenv').config();
const connection = require('../routes/bdConn');
const verifyTokens = require('../auth/verifyTokens');



module.exports = function (app) {
    app.post('/bindUserGroup', verifyTokens, (req, res) => {
        console.log(req.body);

        let queries = [];

        req.body.users.forEach((user) => {
            req.body.groups.forEach((group) => {
                queries.push(`insert into users_bind(users_bind.uid, users_bind.gid) values (${user.id}, ${group.id});`);
            })
        });

        let sendRes = [];
        queries.forEach(query => {
            connection.query(query, (err, results, fields) => {
                if (err) {
                    console.log(err);
                } else {
                    sendRes.push(results)
                }

            });
        });
        res.send(sendRes);
    });

    app.post('/bindGroupSub', (req, res) => {

        let query = `insert into groups_subscribe(groups_subscribe.gid, groups_subscribe.sid, groups_subscribe.date) ` +
            `values (${req.body.groupId}, ${req.body.subId}, ${req.body.date});`;

        connection.query(query, (err, results, fields) => {
            if (err) {
                console.log(err);
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    app.post('/bindPackCourses', (req, res) => {

        let query = `insert into courses_pack(courses_pack.pid, courses_pack.cid, courses_pack.excluded_works) ` +
            `values (${req.body.packageId}, ${req.body.courseId}, "${req.body.excluded_works}");`;

        connection.query(query, (err, results, fields) => {
            if (err) {
                console.log(err);
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });

    app.post('/bindSchedulesPack', (req, res) => {

        console.log(req.body)

        let query = `insert into subscriptions(subscriptions.pid, subscriptions.sid, subscriptions.desc) ` +
            `values (${req.body.packId}, ${req.body.scheduleId}, '${req.body.desc}');`;

        connection.query(query, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }

        });
    });
};