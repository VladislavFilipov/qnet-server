require('dotenv').config();
const connection = require('../routes/bdConn');
const verifyTokens = require('../auth/verifyTokens');

module.exports = function (app) {
    // добавить пакет
    app.post('/packages/add', (req, res) => {
        let query = `insert into packages(packages.title, packages.desc) values("${req.body.pack.title}", "${req.body.pack.desc}");`
        connection.query(query, (err, results, fields) => {
            if (err) {
                res.send({ 'error': err });
            } else {
                res.send(results);
            }
        });
    });
};