const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 4001;

app.use(bodyParser.json());
app.use(cors({ origin: 'http://localhost:3000' }));
app.use("/static", express.static(__dirname + "/static"));

const route = require('./routes/index');
route(app);

app.listen(port, () => {
    console.log('Запущено. Порт: ' + port);
});
