const jwt = require('jsonwebtoken');
const connection = require('../routes/bdConn');

const getSessionByAccessToken = (token) => connection.promise()
    .query(`select * from ${process.env.DB_NAME}.sessions_admin where accessToken = '${token}';`)
    .then(([rows, _]) => rows.length == 0 ? null : rows[0])
    .catch(console.log);

module.exports = async (req, res, next) => {
    const accessToken = req.headers.authorization.split(' ')[1];

    try {
        await jwt.verify(accessToken, `${process.env.ACCESS_TOKEN_SECRET_KEY}`);

        const session = await getSessionByAccessToken(accessToken);

        if (session) {
            if (session.expiresIn > Date.now() / 1000) next();
            else res.status(401).send({ message: 'not authed' });
        } else {
            res.status(401).send({ message: 'not authed (session)' });
        }

    } catch (error) {
        console.log(error);
        res.status(401).send({ message: error });
    }
}