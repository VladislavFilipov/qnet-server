require('dotenv').config();
const jwt = require('jsonwebtoken');
const saltedMd5 = require('salted-md5');
const randtoken = require('rand-token');

const connection = require('../routes/bdConn');
const verifyTokens = require('./verifyTokens');

const findCandidate = (login) => connection.promise()
    .query(`select * from ${process.env.DB_NAME}.users_local where login = '${login}';`)
    .then(([rows, _]) => rows.length == 0 ? null : rows[0])
    .catch(console.log);

const getSessionByRefreshToken = (token) => connection.promise()
    .query(`select * from ${process.env.DB_NAME}.sessions_admin where refreshToken = '${token}';`)
    .then(([rows, _]) => rows.length == 0 ? null : rows[0])
    .catch(console.log);

module.exports = function (app) {
    app.post('/auth/login', async function (req, res) {

        const candidate = await findCandidate(req.body.login);

        if (candidate) {

            const saltedHash = saltedMd5(req.body.password, candidate.p_salt);

            if (saltedHash === candidate.p_hash) {

                connection.query(`select * from ${process.env.DB_NAME}.sessions_admin where login = '${candidate.login}';`, (err, results, fields) => {
                    if (err) {
                        console.log("error ", err);
                    } else {
                        const
                            expiresIn = Math.floor((Date.now() + (60 * 5 * 1000)) / 1000),
                            accessToken = jwt.sign({ login: candidate.login, expiresIn }, `${process.env.ACCESS_TOKEN_SECRET_KEY}`),
                            refreshToken = randtoken.generate(16);

                        console.log(refreshToken)

                        if (results.length == 0) {
                            connection.query(`insert ${process.env.DB_NAME}.sessions_admin(login, accessToken, refreshToken, expiresIn) values ('${candidate.login}', '${accessToken}', '${refreshToken}', ${expiresIn});`,
                                (err, results, fields) => {
                                    if (err) res.status(401).send({ message: 'error' });
                                    else res.status(200).send({ accessToken, refreshToken, expiresIn });
                                }
                            );

                        } else {
                            connection.query(`update ${process.env.DB_NAME}.sessions_admin set accessToken = '${accessToken}', refreshToken = '${refreshToken}', expiresIn = ${expiresIn} where login = '${candidate.login}';`,
                                (err, results, fields) => {
                                    if (err) res.status(401).send({ message: 'error' });
                                    else res.status(200).send({ accessToken, refreshToken, expiresIn });
                                }
                            );
                        }
                    }
                });
            } else {
                res.status(401).send({
                    message: 'Неверный пароль.'
                });
            }
        } else {
            res.status(404).send({
                message: 'Пользователь не найден.'
            });
        }
    });

    app.post('/auth/refresh', async function (req, res) {

        if (req.body.refreshToken) {

            const session = await getSessionByRefreshToken(req.body.refreshToken);

            const
                expiresIn = Math.floor((Date.now() + (60 * 5 * 1000)) / 1000),
                accessToken = jwt.sign({ login: session.login, expiresIn }, `${process.env.ACCESS_TOKEN_SECRET_KEY}`),
                refreshToken = randtoken.generate(16);

            connection.query(`update ${process.env.DB_NAME}.sessions_admin set accessToken = '${accessToken}', refreshToken = '${refreshToken}', expiresIn = ${expiresIn} where refreshToken = '${req.body.refreshToken}';`,
                (err, results, fields) => {
                    if (!results.changedRows) res.status(401).send({ message: 'error' });
                    else res.status(200).send({ accessToken, refreshToken, expiresIn });
                }
            );
        } else res.status(401).send();

    });

    app.get('/auth/is-authed', verifyTokens, function (req, res) {
        res.status(200).send();
    });
}